<?php
/**
 * A little script that helps to upload a directory through FTP by ZIPing the files, send them over FTP and then unZIPing then on the server.
 * 
 * @author Antoine du HAMEL
 * @category Dev
 * 
 */
define('CONNECTION_REQUIRED', 1);

define('FTP_HOST', 'ftp.exemple.com');
define('FTP_LOGIN', 'user');

define('REMOTE_FILE', 'www/dev/tmp.zip'); // name of the zip on your distant server
define('REMOTE_SCRIPT', 'exemple.com/dev/unzip.php'); // url of the script on your distant server

// The name of the directory where your source files are
define('PROJECT_FOLDER', __DIR__.DIRECTORY_SEPARATOR.'mysite');
define('PROJECT_ROOT','mysite');

define('LAST_UPDATE', 0);

function fullPath($dir)
{
	return PROJECT_FOLDER.DIRECTORY_SEPARATOR.$dir;
}

if ((empty($_POST['mdp']) && !isset($_POST['preview'])) || empty($_POST['mindate']) || empty($_POST['dir']))
{
	?><html><head><title>Upload to distant server</title><style type="text/css">code{color:#c7254e;background-color:#f9f2f4;}</style></head><body>
	<p>This script will upload to your FTP server the files from your project (except the configuration ones). Please be patient and click <strong>only once</strong> on the upload button. The steps are :</p>
	<ol>
		<li>Zip all the files</li>
		<li>Connect to your FTP server <em>(if it fails, please check the settings and the password)</em></li>
		<li>Upload the ZIP <em>(if it fails, please check if your distant server has enough storage)</em></li>
		<li>Delete the ZIP from your local server</li>
		<li>Unzip it on your server <em>(if a 404 error is triggered, please check if you&#39;ve placed the unzip script on yourt distant server)</em></li>
		<li>Delete it from your server</li>
		<li>Tell you it's <code>DONE</code> <em>(if the message is <code>FAIL</code>, maybe it means PHP has not the permission level granted)</em></li>
	</ol>
	<p><strong>WARNING : This will <u>overwritten existing files</u> on the distant server by the version on your local server. You have to be sure that your code is ready for production!</strong><br>
	Otherwise, the files you have deleted on your local server will still remain on the distant server, this script cannot remove them!<br>
	If you are uploading all of the files of your project, the process may take a while, depending on your connection.</p>
	<form method="post"><p>
		<label for="mdp">FTP Password for <code><?php echo FTP_LOGIN.'@'.FTP_HOST; ?></code> : </label><input type="password" name="mdp" id="mdp" autofocus /><br/>
		<label>Minimum date of last modifcation for a file to be uploaded : <input type="datetime-local" name="mindate" value="<?php echo date('Y-m-d\TH:i',LAST_UPDATE);?>"/></label><br/>
		<label><input type="checkbox" name="preview" /> Preview files to be uploaded</label><br/>
		<p>Choose the directories to upload :</p>
		<?php
		$root_dir = array_diff(array_filter(scandir(PROJECT_FOLDER), function($cv){
			return is_dir(fullPath($cv));
		}), array('..', '.', '_tmp', '_dist', 'upl0', '.git'));
		foreach ($root_dir as $name)
			echo '<label><input type="checkbox" name="dir[]" value="'.$name.'" checked/>'.$name.'</label><br/>';
		?>
		<br/><input type="submit" value="Upload" />
	</p></form></body></html><?php
}
else
{
	$GLOBALS['fileCount'] = 0;
	/**
	 * Put all the files and recursively the directories of a directory into an array
	 * @param string $dir Absolute path of the directory
	 * @return array
	 */
	function dirToArray($dir) 
	{    
		$result = array(); 

		$cdir = array_diff(scandir($dir), array('..', '.')); 
		foreach ($cdir as $key => $value) 
		{
			$cv = $dir . DIRECTORY_SEPARATOR . $value;
			if (is_dir($cv))
				$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value); 
			else if(filemtime($cv)>strtotime($_POST['mindate']))
				$GLOBALS['fileCount']+=!!($result[] = $value);
		}

		return $result; 
	}

	/**
	 * Zip all the files within the arborescence of the array given
	 * 
	 * @param ZipArchive $zip
	 * @param array $array Arborescence of the files & directories to ZIP
	 * @param string $name Name of the parent ziped-directory
	 * @param string $path Absolute path of the parent directory
	 * 
	 * @return void
	 */
	function arrayToZip($zip, $array, $name, $path)
	{
		if(!empty($name))
			$zip->addEmptyDir($name);
		foreach ($array as $dir_name=>$file_name)
		{
			if (is_array($file_name))
				arrayToZip($zip, $file_name, $name.'/'.$dir_name, $path.DIRECTORY_SEPARATOR.$dir_name);
			else
				$zip->addFile($path.DIRECTORY_SEPARATOR.$file_name, $name.'/'.$file_name);
		}
	}


	$files = array_reduce(array_filter(array_map('fullPath', $_POST['dir']), 'is_dir'), function ($pv, $cv) {
			$pv[substr($cv, strlen(fullPath('')))]=dirToArray($cv);
			return $pv;
		}, array());

	if(isset($_POST['preview']))
		exit('<a href="javascript:history.back();">Go back!</a><br/><b>'.$GLOBALS['fileCount'].' files selected</b><pre>'.str_replace('Array', 'Directory', print_r($files, true)).'</pre>');

	set_time_limit(120);
	$ftp=ftp_connect(FTP_HOST) or exit('FTP server unreachable!');


	ftp_login($ftp, FTP_LOGIN, $_POST['mdp']) or exit('Couldn\'t connect to FTP!');

	$file = tempnam(sys_get_temp_dir(),'sav');
	$zip = new ZipArchive;

	if($zip->open($file, ZipArchive::CREATE)!==true)
		throw new Exception('Impossible de créer l\'archive', 1);
		
	arrayToZip($zip,
		$files, 
		PROJECT_ROOT, 
		PROJECT_FOLDER);

	$zip->close();

	ftp_pasv($ftp, true);
	ftp_put($ftp, REMOTE_FILE, $file, FTP_BINARY);// or exit('File transfert failed');

	header('Location: http://'.REMOTE_SCRIPT);
	ftp_close($ftp);
	unlink($file);
	
	file_put_contents(__FILE__, 
		str_replace(
			'define(\'LAST_UPDATE\', '.LAST_UPDATE.');', 
			'define(\'LAST_UPDATE\', '.$_SERVER['REQUEST_TIME'].');', 
			file_get_contents(__FILE__)));
}
