<?php
/**
 * Unzip files send by zip4upload.php
 * A little script that helps to upload a directory through FTP by ZIPing the files, send them over FTP and then unZIPing then on the server.
 * 
 * @author Antoine du HAMEL
 * @category Prod
 * 
 */
define('FILE_PATH', 'tmp.zip');
define('FILE_DEST', $_SERVER['DOCUMENT_ROOT']);

$zip = new ZipArchive;

if ($zip->open(FILE_PATH)===true && $zip->extractTo(FILE_DEST))
{
    $size = filesize(FILE_PATH);
    echo $size.' bytes transfered ('.($size>>10).' Kio or '.($size>>20).' Mio).<br>'."\n";
    unlink(FILE_PATH);
    $zip->close();
    exit('SUCCESS!');
}

exit('FAIL');
